#Conversion Tracking Kludge for Aurora

This script is a kludge that makes it possible to do Google conversion tracking in (technically out of) Aurora.

1.  An outside event (that will ultimately be tracked) redirects to this script
2.  Script gets conversion code variables from query string
3.  Script builds the conversion code using the passed variables

    +   Logical defaults are set for any variables that are not passed in query string

4.  Page is loaded by browser, tracking code executes, user is given a link back to the referer URL

##Setup process in Gravity Forms:

1.  Make a new Gravity Form, include the following hidden fields:

    +   [HTTP referer URL] with a default value of {referer}
    +   [Submit Date] with a default value of {date_mdy}

2.  Change Default Confirmation to type Redirect

    +   Set the Redirect URL to the address of this script
    +   Check 'Pass Field Data Via Query String'

3.  Set conversion code variables in Query String:
    
    +   `google_conversion_id=[YOUR CONVERSION ID HERE]&google_conversion_label=[YOUR CONVERSION LABEL HERE]&http_referer_url={HTTP referer URL:4}&name={Name:2}&program=[YOUR PROGRAM NAME HERE]`
    +   **NOTE:** the numbers after the tags (ie {Name:2}) might have to be changed. Check them.
    +   google_conversion_id and google_conversion_label are required, other fields are set as defaults below
    +   **Optional but recommended:** include 'name' in the query string, setting it to the value recieved by the form (use the dropdown to get the tag)
    +   **Optional but recommended:** include 'program' in the query string, set it to the program title, spaces and caps are ok.