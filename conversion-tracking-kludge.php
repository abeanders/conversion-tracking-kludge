<!DOCTYPE html>
<html>
<head>
<?php



/**
 *  Google Conversion Tracking for Aurora
 *  =====================================
 *
 *  This script is a kludge that makes it possible to do Google conversion tracking in (technically out of) Aurora.
 *  
 *  1) An outside event (that will ultimately be tracked) redirects to this script
 *  2) Script gets conversion code variables from query string
 *  3) Script builds the conversion code using the passed variables
 *      3a) Logical defaults are set for any variables that are not passed in query string
 *  4) Page is loaded by browser, tracking code executes, user is given a link back to the referer URL
 *
 *  
 *  Setup process in Gravity Forms:
 *  
 *  1) Make a new Gravity Form, include the following hidden fields:
 *      1a) [HTTP referer URL] with a default value of {referer}
 *      1b) [Submit Date] with a default value of {date_mdy}
 *  2) Change Default Confirmation to type Redirect
 *      2a) Set the Redirect URL to the address of this script
 *      2b) Check 'Pass Field Data Via Query String'
 *  3) Set conversion code variables in Query String:
 *      3a) google_conversion_id=[YOUR CONVERSION ID HERE]&google_conversion_label=[YOUR CONVERSION LABEL HERE]&http_referer_url={HTTP referer URL:4}&name={Name:2}&program=[YOUR PROGRAM NAME HERE]
 *      3b) NOTE: the numbers after the tags (ie {Name:2}) might have to be changed. Check them.
 *      3c) google_conversion_id and google_conversion_label are required, other fields are set as defaults below
 *      3d) Optional but recommended: include 'name' in the query string, setting it to the value recieved by the form (use the dropdown to get the tag)
 *      3e) Optional but recommended: include 'program' in the query string, set it to the program title, spaces and caps are ok.
 *
 *
 *      ~Abe Millett
 */



// set default values...
    $default_vals = array(
        'google_conversion_id'          => 'not_set',
        'google_conversion_language'    => 'en',
        'google_conversion_format'      => 2,
        'google_conversion_color'       => 'dadada',
        'google_conversion_label'       => 'not_set',
        'google_conversion_value'       => 0,
        'google_remarketing_only'       => false,
        'name'                          => '',
        'program'                       => ''
    );


// get $_GET and clean it up...
    $args = array(
        'google_conversion_id'          => FILTER_VALIDATE_INT,
        'google_conversion_language'    => FILTER_SANITIZE_ENCODED,
        'google_conversion_format'      => array(
                                                'filter'  => FILTER_VALIDATE_INT,
                                                'options' => array( 'min_range' => 1, 'max_range' => 3 )
                                            ),
        'google_conversion_color'       => FILTER_VALIDATE_INT,
        'google_conversion_label'       => FILTER_SANITIZE_ENCODED,
        'google_conversion_value'       => FILTER_VALIDATE_INT,
        'google_remarketing_only'       => FILTER_VALIDATE_BOOLEAN,
        'name'                          => FILTER_SANITIZE_ENCODED,
        'program'                       => FILTER_SANITIZE_STRING
    );
    $get_clean = filter_input_array( INPUT_GET, $args );


// strip query string of referer URL...
    $http_referer = parse_url( $_SERVER[ 'HTTP_REFERER' ] );


// merge $get_clean with $default to get $values...
    $values = array();
    // ignore any values passed that do not have a corresponding key in $default_vals...
    foreach ( $default_vals as $key => $val ) {
        $values[ $key ] = array_key_exists( $key, $_GET ) ? $get_clean[ $key ] : $val;
    }


// make the page look nice for the unwitting viewer...
    ?>
    <style><!--

        html { background: #<?php echo( $values[ 'google_conversion_color' ] ); ?>; }
        body {
            width: 1000px;
            margin: 48px auto;
            text-align: center;
            color: #5a5a5a;
        }
        a { color: #0ad; }
        p { margin-top: 48px; }

    --></style>
    <?php


// make a nice page title...
    ?>
        <title>Information Request<?php if ( $values[ 'name' ] != '' ) echo( 'ed by ' . ucwords( urldecode( $values[ 'name' ] ) ) ); ?></title>
    <?php


// display friendly and helpful text to the unwitting viewer...
    ?>
        </head>
        <body>

        <h1>Thank you for your request<?php if ( $values[ 'name' ] != '' ) echo( ', ' . ucwords( urldecode( $values[ 'name' ] ) ) ); ?>!</h1>
        <h2>We will respond as promptly as possible.</h2>

        <h1><a href="<?php echo( $_SERVER[ 'HTTP_REFERER' ] ); ?>">Go back to the page you were on</a>.</h1>

        <p>
            <a href="http://<?php echo( $http_referer[ 'host' ] ); ?>"><?php echo( $values[ 'program' ] ); ?></a>
        </p>
    <?php


// echo conversion tracking code if google_conversion_id and google_conversion_label have been set...
    if ( $values[ 'google_conversion_id' ] != 'not_set' && $values[ 'google_conversion_label' ] != 'not_set' ) {
    ?>
        <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = <?php echo( $values[ 'google_conversion_id' ] ); ?>;
            var google_conversion_language = <?php echo( '"' . $values[ 'google_conversion_language' ] . '"' ); ?>;
            var google_conversion_format = <?php echo( '"' . $values[ 'google_conversion_format' ] . '"' ); ?>;
            var google_conversion_color = <?php echo( '"' . $values[ 'google_conversion_color' ] . '"' ); ?>;
            var google_conversion_label = <?php echo( '"' . $values[ 'google_conversion_label' ] . '"' ); ?>;
            var google_conversion_value = <?php echo( $values[ 'google_conversion_value' ] ); ?>;
            var google_remarketing_only = <?php var_export( $values[ 'google_remarketing_only' ] ); ?>;
            /* ]]> */
        </script>
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>

        <noscript>
            <div style="display:inline;">
                <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/<?php echo( $values[ 'google_conversion_id' ] ); ?>/?value=<?php echo( $values[ 'google_conversion_value' ] ); ?>&label=<?php echo( $values[ 'google_conversion_label' ] ); ?>&guid=ON&script=0"/>
            </div>
        </noscript>
    <?php
    }


    ?>

</body>
</html>